from flask import request
from flask_restplus import Resource
from api import api
from api.matricula.matricula import Matricula

ns = api.namespace('matricula', description='Matricula')

@ns.route('/listarAlunoPerdidos/<int:co_entidade>')
class listarAlunoPerdidosAPP(Resource):

    @ns.response(code=400, description="Bad Request")
    def get(self, co_entidade):
        m = Matricula()
        resp = m.qtdAlunosPerdidos(co_entidade)
        return resp

@ns.route('/listarAlunoGanhos/<int:co_entidade>')
class listarAlunoGanhosAPP(Resource):

    @ns.response(code=400, description="Bad Request")
    def get(self, co_entidade):

        m = Matricula()
        resp = m.qtdAlunosGanhos(co_entidade)
        return resp

@ns.route('/criarTabelaMatricula')
class criarTabelaMatriculaAPP(Resource):

    @ns.response(code=400, description="Bad Request")
    def get(self):

        m = Matricula()
        resp = m.criarTabelaMatricula()
        return resp

@ns.route('/popularTabelaMatricula')
class popularTabelaMatriculaAPP(Resource):

    @ns.response(code=400, description="Bad Request")
    def get(self):

        m = Matricula()
        resp = m.populaTabelaMatricula()
        return resp
