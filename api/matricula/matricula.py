import csv
import os
from database.conexao import conexao

class Matricula():
    #FUNCAO QUE CRIA A TABELA DE MATRICULA
    def criarTabelaMatricula(self):
        cursor = conexao.cursor()
        self.nomeTabela = 'tb_matricula'

        try:
            cursor.execute('''CREATE TABLE {} (
                            id_matricula BIGINT,
                            nu_ano_censo int,
                            co_pessoa_fisica bigint,
                            co_entidade bigint,
                            primary key (id_matricula, nu_ano_censo)
                            );'''.format(self.nomeTabela))
        
            conexao.commit()
        except:
            return {'status': False, 'message': "Erro ao criar a tabela."}
 
        return {'status': True, 'message': ''}

    #FUNCAO QUE IRA POPULAR AS MATRICULAS NA BASE DE DADOS
    def populaTabelaMatricula(self):
        self.nomeTabela = 'tb_matricula'
        cursor = conexao.cursor()
        
        #LOOP NA LISTA DE ARQUIVOS DENTRO DA PASTA "dados"
        for arquivo in os.listdir('dados'):
            with open('dados/' + arquivo) as csv_file:
                #ABRE O ARQUIVO
                reader = csv.reader(csv_file, delimiter='|', quoting=csv.QUOTE_NONE)
                cabecalho = True
                for row in reader:
                    #AQUI PEGARA OS INDICES DOS CAMPOS QUE PRECISAMOS NO CABECALHO
                    if cabecalho:
                        try:
                            indexMatricula = row.index('ID_MATRICULA')
                            indexEntidade = row.index('CO_ENTIDADE')
                            indexAno = row.index('NU_ANO_CENSO')
                            indexPessoaFisica = row.index('CO_PESSOA_FISICA')
                        except:
                            #NAO ENCONTRADA O INDICE, SIGNIFICA QUE O ARQUIVO NAO ESTA CORRETO
                            return {'status': False, 'message': "Arquivo "+  arquivo + " nao esta de acordo o padrao."}
                        cabecalho = False
                    else:
                        #INSERCAO DOS DADOS NA TABELA
                        try:
                            cursor.execute(''' INSERT INTO {} 
                                    (id_matricula, nu_ano_censo, co_pessoa_fisica, co_entidade) VALUES
                                    ({}, {}, {}, {});'''.format(self.nomeTabela, row[indexMatricula], row[indexAno], row[indexPessoaFisica], row[indexEntidade]))
                        
                            conexao.commit()
 
                        except:
                            print("Erro ao inserir este dado")
                    
        return {'status': True, 'message': ''}
    
    #FUNCAO QUE RETORNA QUAIS ALUNOS ESTAO MATRICULADOS NO ANO X E NAO MATRICULADOS NO ANO Y
    def listarAlunos(self, co_entidade, anoInicial, anoFinal):
        cursor = conexao.cursor()
        self.nomeTabela = 'tb_matricula'

        try:
            cursor.execute('''
                SELECT * FROM {}
                    WHERE co_entidade = {}
                        AND nu_ano_censo = {}
                        AND co_pessoa_fisica NOT IN(
                             SELECT co_pessoa_fisica FROM {}
                                WHERE co_entidade = {}
                                    AND nu_ano_censo = {}
                        )
                        '''.format(self.nomeTabela, co_entidade, anoInicial, self.nomeTabela, co_entidade, anoFinal))
        
            resultado = cursor.fetchall()

        except:
            return {'status': False, 'message': "Erro ao fazer a consulta."}
 
        return {'status': True, 'message': resultado}

    #FUNCAO QUE RETORNA QUANTOS ALUNOS A ESCOLA PERDEU EM 2017
    def qtdAlunosPerdidos(self, co_entidade):
        anoInicial = 2016
        anoFinal = 2017

        r = self.listarAlunos(co_entidade, anoInicial, anoFinal)

        if r['status']:
            return {'quantidadeAlunos': len(r['message'])}, 200
        else:
            return r, 400
    
    #FUNCAO QUE RETORNA QUANTOS ALUNOS A ESCOLA GANHOU EM 2017
    def qtdAlunosGanhos(self, co_entidade):
        anoInicial = 2017
        anoFinal = 2016

        r = self.listarAlunos(co_entidade, anoInicial, anoFinal)

        if r['status']:
            return {'quantidadeAlunos': len(r['message'])}, 200
        else:
            return r, 400

    #FUNCAO QUE RETORNA O SALDO DE QUANTOS ALUNOS A ESCOLA GANHOU - PERDEU NO ANO DE 2017
    def saldoAlunosGanhosPerdidos(self, co_entidade):
        r1 = self.qtdAlunosGanhos(co_entidade)
        r2 = self.qtdAlunosPerdidos(co_entidade)
        try:
            return r1[0]['quantidadeAlunos'] -  r2[0]['quantidadeAlunos']
        except:
            return 0

