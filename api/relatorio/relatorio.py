from api.matricula.matricula import Matricula
from database.conexao import conexao

class Relatorio():
    
    def primeiroRelatorio(self, co_entidade):
        #INICIANDO AS VARIAVEIS
        m = Matricula()
        anoInicial = 2017
        anoFinal = 2016
        cursor = conexao.cursor()
        self.nomeTabela = 'tb_matricula'

        #OBTENDO SALDO DE ALUNOS DA ESCOLA
        saldo = m.saldoAlunosGanhosPerdidos(co_entidade)
        
        #OBTENDO LISTA DE ESCOLAS NOVAS DOS ALUNOS
        try:
            cursor.execute('''
                SELECT distinct co_entidade FROM {}
                    WHERE co_entidade <> {}
                        AND nu_ano_censo = {}
                        AND co_pessoa_fisica IN(
                             SELECT co_pessoa_fisica FROM {}
                                WHERE co_entidade = {}
                                    AND nu_ano_censo = {}
                        )
                        '''.format(self.nomeTabela, co_entidade, anoInicial, self.nomeTabela, co_entidade, anoFinal))
        
            resultado = cursor.fetchall()

        except:
            return {'message': "Erro ao fazer a consulta."}, 400

        return {'saldo': saldo, 'listaEscolaDestino': resultado}, 200

        