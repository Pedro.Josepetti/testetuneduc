from flask import request
from flask_restplus import Resource
from api import api
from api.relatorio.relatorio import Relatorio

ns = api.namespace('Relatorio', description='Relatorios')

@ns.route('/relatorio/<int:co_entidade>')
class relatorioAPP(Resource):

    @ns.response(code=400, description="Bad Request")
    def get(self, co_entidade):
        r = Relatorio()
        resp = r.primeiroRelatorio(co_entidade)
        return resp