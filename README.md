Ferramentas Utilizadas:

Python 3
https://www.python.org/downloads/

MySQL
https://dev.mysql.com/downloads/installer/

-> PIP:
*******************************************************
Após instalar o Python, será necessário intalar o pip:

    Windowns: Rodar no terminal "python get-pip.py" dentro do diretório do projeto.
    Linux: Rodar no terminal "sudo apt install python3-pip"
*******************************************************

-> Configurando acesso a base:
*******************************************************
Após instalar MySQL e criar suas credenciais, 
é necessário informar login, senha o nome da database criado no arquivo: "database/conexao.py",
para permirtir o acesso ao banco de dados.

(db='NOME_DA_BASE', user='LOGIN', passwd='SENHA')
*******************************************************

-> Instalando bibliotecas:
*******************************************************
Em seguida, para intalar as bibliotecas dependentes,executar o seguinte comando pelo terminal:
    - python setup.py develop
*******************************************************

-> Iniciando API:
*******************************************************
Enfim executar o seguinte comando pelo terminal para rodar a API:
    - python app.py

A API estará rodando. Se entrar por qualquer browser no link : "localhost:5400", irá entrar no swagger da API
ou fazer as chamadas via POSTMan.
*******************************************************

-> Criando Tabela:
*******************************************************
Executar na API caminho:  "/matricula/criarTabelaMatricula" (GET)

*******************************************************

-> Populando Tabela:
*******************************************************
Executar na API caminho:  "/matricula/popularTabelaMatricula" (GET)

Lembrando que o arquivo deverá ta localizado na pasta "dados"

*******************************************************

-> Rodando Relatório:
*******************************************************
Executar na API caminho:  "/Relatorio/relatorio/{co_entidade}" (GET)

Lembrando que no lugar de {co_entidade}, deverá ser o número da entidade da escola.
*******************************************************