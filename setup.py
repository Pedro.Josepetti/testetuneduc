from setuptools import setup

# install requirements of the project
with open("requirements.txt") as req:
    install_requires = req.read()

setup(name='testeTuneduc',
      version="0.0.1",
      description="API Tuneduc",
      url="https://gitlab.com/Pedro.Josepetti/testetuneduc.git",
      author="Pedro Josepetti",
      author_email="pedro.josepetti@gmail.com.br",
      license="BSD",
      keywords="",
      zip_safe=False,
      install_requires=install_requires)
     
