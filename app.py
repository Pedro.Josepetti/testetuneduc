from flask import Flask, Blueprint
from api import api
from api.matricula import ns as ns_matricula 
from api.relatorio import ns as ns_relatorio

app = Flask(__name__)
blueprint = Blueprint('api', __name__)
api.init_app(blueprint)

api.add_namespace(ns_relatorio, '/relatorio')
api.add_namespace(ns_matricula, '/matricula')
app.register_blueprint(blueprint)

if __name__ == "__main__":
    host = '0.0.0.0'
    port = '5400'
    debug = True
    app.run(host, port, debug)